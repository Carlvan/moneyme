jQuery(document).ready(function () {
  // sidebar close/open
  $('.dismiss').on('click', function () {
    $('.sidebar').removeClass('active');
    $('.open-menu').removeClass('d-none');
    $('#login-main').removeClass('d-none');
  });

  $('.open-menu').on('click', function (e) {
    e.preventDefault();
    $('.sidebar').addClass('active');
    $('.open-menu').addClass('d-none');
    $('#login-main').addClass('d-none');
  });

  //   slider
  // get live slider value
  let amount = document.getElementById('amount');
  let duration = document.getElementById('duration');
  let interestRate = document.getElementById('interestRate');
  let mPayments = document.getElementById('mPayments');
  let tPayments = document.getElementById('tPayments');
  const mFee = 10; // monthly fee = $10

  // initial values
  let amountValue = Number(amount.value);
  let durationValue = Number(duration.value);
  let interestRateValue = Number(interestRate.value) / 100;
  // compute
  computeRepayments(amountValue, durationValue, interestRateValue);

  // override browser colors
  const sliders = document.querySelectorAll('.slider');
  sliders.forEach((slider) => {
    const min = slider.min;
    const max = slider.max;
    const value = slider.value;
    slider.style.background = `linear-gradient(to right, #04ff80 0%, #04ff80 ${
      ((value - min) / (max - min)) * 100
    }%, #fff ${((value - min) / (max - min)) * 100}%, #fff 100%)`;
  });

  // Update the each current slider value (each time you drag the slider handle)
  amount.oninput = function () {
    //   update on value change
    let amountValue = Number(this.value);
    let durationValue = Number(duration.value);
    let interestRateValue = Number(interestRate.value) / 100;
    this.style.background = `linear-gradient(to right, #04ff80 0%, #04ff80 ${
      ((this.value - this.min) / (this.max - this.min)) * 100
    }%, #fff ${
      ((this.value - this.min) / (this.max - this.min)) * 100
    }%, #fff 100%)`;
    // compute
    computeRepayments(amountValue, durationValue, interestRateValue);
  };

  duration.oninput = function () {
    //   update on value change
    let amountValue = Number(amount.value);
    let durationValue = Number(this.value);
    let interestRateValue = Number(interestRate.value) / 100;
    this.style.background = `linear-gradient(to right, #04ff80 0%, #04ff80 ${
      ((this.value - this.min) / (this.max - this.min)) * 100
    }%, #fff ${
      ((this.value - this.min) / (this.max - this.min)) * 100
    }%, #fff 100%)`;
    // compute
    computeRepayments(amountValue, durationValue, interestRateValue);
  };

  interestRate.oninput = function () {
    //   update on value change
    let amountValue = Number(amount.value);
    let durationValue = Number(duration.value);
    let interestRateValue = Number(this.value) / 100;
    this.style.background = `linear-gradient(to right, #04ff80 0%, #04ff80 ${
      ((this.value - this.min) / (this.max - this.min)) * 100
    }%, #fff ${
      ((this.value - this.min) / (this.max - this.min)) * 100
    }%, #fff 100%)`;
    // compute
    computeRepayments(amountValue, durationValue, interestRateValue);
  };

  //   computation formulas
  function computeRepayments(amountValue, durationValue, interestRateValue) {
    mPayments.innerHTML =
      '$' +
      (
        (amountValue + interestRateValue * amountValue + mFee * durationValue) /
        durationValue
      )
        .toFixed(2)
        .toLocaleString('en-US', { maximumFractionDigits: 2 });
    tPayments.innerHTML =
      '$' +
      (
        amountValue +
        interestRateValue * amountValue +
        mFee * durationValue
      ).toLocaleString('en-US', { maximumFractionDigits: 2 });
  }

  //   show values as you slide the slider
  const allRanges = document.querySelectorAll('.slidecontainer');
  allRanges.forEach((wrap) => {
    const range = wrap.querySelector('.slider');
    const outputContainer = wrap.querySelector('.output');
    const output = wrap.querySelector('.output span');

    range.addEventListener('input', () => {
      setBubble(range, output, outputContainer);
    });
    setBubble(range, output, outputContainer);
  });

  function setBubble(range, output, outputContainer) {
    const val = range.value;
    const min = range.min ? range.min : 0;
    const max = range.max ? range.max : 100;
    const newVal = Number(((val - min) * 100) / (max - min));
    output.innerHTML = Number(val).toLocaleString('en-US', {
      maximumFractionDigits: 2,
    });

    // position adjusted based on design
    outputContainer.style.left = `calc(${newVal}% + (${
      -37 - newVal * 0.15
    }px))`;
  }
});
